//
//  PageViewController.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 27/05/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    
    var headersArray = ["Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since unchanged.", "It is a long established fact that a reader will be distracted by the readable content", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since unchanged."]
    var imagesArray = ["welcome_1","welcome_2", "welcome_3"]
    
    let pageContol: UIPageControl = {
        let pageContol = UIPageControl()
        pageContol.isUserInteractionEnabled = false
        pageContol.translatesAutoresizingMaskIntoConstraints = false
        return pageContol
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        setupPageControl()
        
        if let firstVC = displayViewContoller(atIndex: 0) {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    private func setupPageControl() {
        pageContol.numberOfPages = headersArray.count
        view.addSubview(pageContol)
        NSLayoutConstraint.activate([
            pageContol.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 16),
            pageContol.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -18)
            ])
    }
    
    func displayViewContoller(atIndex index: Int) -> ContentViewController? {
        guard index >= 0 else { return nil }
        guard index < headersArray.count else { return nil }
        guard let contentVC = storyboard?.instantiateViewController(withIdentifier: "contentViewController") as? ContentViewController else { return nil }
        
        contentVC.imageFile = imagesArray[index]
        contentVC.header = headersArray[index]
        contentVC.index = index
        
        return contentVC
    }
    
    func nextVC(atIndex index: Int) {
        if let contentVC = displayViewContoller(atIndex: index + 1) {
            setViewControllers([contentVC], direction: .forward, animated: true, completion: nil)
            pageContol.currentPage = index + 1
        }
    }
    
}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ContentViewController).index
        index -= 1
        return displayViewContoller(atIndex: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ContentViewController).index
        index += 1
        return displayViewContoller(atIndex: index)
    }
    
}

extension PageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let pageVC = pendingViewControllers[0] as? ContentViewController {
            pageContol.currentPage = pageVC.index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            if let pageVC = previousViewControllers[0] as? ContentViewController {
                pageContol.currentPage = pageVC.index
            }
        }
    }
}
