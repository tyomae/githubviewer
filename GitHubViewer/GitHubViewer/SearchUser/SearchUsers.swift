//
//  SearchUsers.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 14/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//


import Foundation

struct SearchUser {
    var login: String
    var avatarURL: String
    var htmlURL: String
    
    init?(dict: [String: AnyObject]) {
        guard let login = dict["login"] as? String,
            let avatarURL = dict["avatar_url"] as? String,
            let htmlURL = dict["html_url"] as? String else { return nil }
        
        self.login = login
        self.avatarURL = avatarURL
        self.htmlURL = htmlURL
    }
}
