//
//  MainViewController.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 08/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices


class SearchUsersViewController: UITableViewController, UISearchBarDelegate, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchUsers = [SearchUser]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup delegates
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate =  self
        tableView.keyboardDismissMode =  .onDrag
        //view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder() // hides the keyboard.
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchUsers.removeAll()
            tableView.reloadData()
            return
        }
        SearchUsersNetworkService.getSearchUsers(searchText: searchText) { (result) in
            self.searchUsers = result.searchUsers
            self.tableView.reloadData()
        }
       
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchUsers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        let searchUser = searchUsers[indexPath.row]
        
        cell.nameLabel.text = searchUser.login
        cell.imageUserView.sd_setImage(with:  URL(string: searchUser.avatarURL), completed: nil)
        return cell
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let urlSafari = searchUsers[indexPath.row]
        let safariVC = SFSafariViewController(url: URL(string: urlSafari.htmlURL)!)
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: nil)
    }
    
    private func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

