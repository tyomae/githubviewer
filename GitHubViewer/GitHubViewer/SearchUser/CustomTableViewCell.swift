//
//  CustomTableViewCell.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 08/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageUserView: UIImageView!
    
    
}
