//
//  SearchUsersNetworkService.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 14/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation
import Alamofire

struct SearchUsersNetworkService {
    
    
    static func getSearchUsers(searchText: String, competion: @escaping(GetSearchUsersResponse) -> ()) {
        guard let url = URL(string: "https://api.github.com/search/users?q=\(searchText)") else { return }
     
        
        NetworkService.shared.getData(url: url) { (json) in
            do {
                let response = try GetSearchUsersResponse(json: json)
                competion(response)
            } catch {
                print(error)
            }
        }
    }
}
