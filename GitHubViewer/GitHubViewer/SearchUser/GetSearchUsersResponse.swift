//
//  GetSearchUsersResponse.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 14/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation

struct GetSearchUsersResponse {
    typealias JSON = [String: AnyObject]
    let searchUsers: [SearchUser]
    
    init(json: Any)  throws {
        guard let jsonObject = json as? JSON,
            let items = jsonObject["items"] as? [JSON] else {throw NetworkError.jsonError }
        
        var searchUsers = [SearchUser]()
        for dictionary in items {
            guard let searchUser = SearchUser(dict: dictionary) else { continue }
            searchUsers.append(searchUser)
        }
        self.searchUsers = searchUsers
    }
    
}
