//
//  SearchRepositoriesNetworkService.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 19/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation
import Alamofire

struct SearchRepositoriesNetworkService {
    
    
    static func getSearchRepositories(searchText: String, competion: @escaping(GetSearchRepositoriesResponse) -> ()) {
        guard let url = URL(string: "https://api.github.com/search/repositories?q=\(searchText)") else { return }
        
        
        NetworkService.shared.getData(url: url) { (json) in
            do {
                let response = try GetSearchRepositoriesResponse(json: json)
                competion(response)
            } catch {
                print(error)
            }
        }
    }
}
