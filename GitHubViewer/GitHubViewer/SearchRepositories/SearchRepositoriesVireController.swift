//
//  SearchRepositoriesVireController.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 19/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit

class SearchRepositoriesViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchRepositories = [SearchRepositorier]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup delegates
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate =  self
        tableView.keyboardDismissMode = .onDrag
        view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder() // hides the keyboard.
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchRepositories.removeAll()
            tableView.reloadData()
            return
        }
        SearchRepositoriesNetworkService.getSearchRepositories(searchText: searchText) { (result) in
            self.searchRepositories = result.searchRepositories
            self.tableView.reloadData()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchRepositories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repositoryCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RepositoriesCustomTableViewCell
        let searchRepositorier = searchRepositories[indexPath.row]
        
        repositoryCell.repositoryLabel.text = searchRepositorier.name
        repositoryCell.fullNameLabel.text = searchRepositorier.fullName
        return repositoryCell
    }
    
    
}

