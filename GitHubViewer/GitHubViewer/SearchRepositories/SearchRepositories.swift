//
//  SearchRepositories.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 19/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation

struct SearchRepositorier {
    var name: String
    var fullName: String
    
    init?(dict: [String: AnyObject]) {
        guard let name = dict["name"] as? String,
            let fullName = dict["full_name"] as? String else { return nil }
        
        self.name = name
        self.fullName = fullName
    }
}

