//
//  GetSearchRepositoriesResponse.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 19/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation

struct GetSearchRepositoriesResponse {
    typealias JSON = [String: AnyObject]
    let searchRepositories: [SearchRepositorier]
    
    init(json: Any)  throws {
        guard let jsonObject = json as? JSON,
            let items = jsonObject["items"] as? [JSON] else {throw NetworkError.jsonError }
        
        var searchRepositories = [SearchRepositorier]()
        for dictionary in items {
            guard let searchRepositorier = SearchRepositorier(dict: dictionary) else { continue }
            searchRepositories.append(searchRepositorier)
        }
        self.searchRepositories = searchRepositories
    }
}
