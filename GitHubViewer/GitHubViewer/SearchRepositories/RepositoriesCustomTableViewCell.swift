//
//  RepositoriesTableViewCell.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 19/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit

class RepositoriesCustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var repositoryLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    
       
}
