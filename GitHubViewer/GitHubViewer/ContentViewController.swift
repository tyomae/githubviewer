//
//  ContentViewController.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 27/05/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController {

    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var pageButton: UIButton!
    
    var header = ""
    var imageFile = ""
    var index = 0
        
    @IBAction func pageButtonPressed(_ sender: UIButton) {
        switch index {
        case 0...1:
            let pageVC = parent as! PageViewController
            pageVC.nextVC(atIndex: index)
        case 2:
            let userDefaults = UserDefaults.standard
            userDefaults.setValue(true, forKey: "wasIntroWatched")
            userDefaults.synchronize()
            dismiss(animated: true, completion: nil)
        default:
            break
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.text = header
        imageView.image = UIImage(named: imageFile)
        
        switch index {
        case 0...1: pageButton.setTitle("Next", for: .normal)
        case 2: pageButton.setTitle("Done", for: .normal)
        default:
            break
        }
    }
    


}
