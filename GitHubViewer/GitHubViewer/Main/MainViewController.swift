//
//  MainViewController.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 18/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation
import UIKit


class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var searchUsers = [SearchUser]()
    let gitHub: [String] = ["Search Users", "Search Repositories"]
    let cellReuseIdentifier = "Cell"
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let userDefaults = UserDefaults.standard
        let wasIntroWatched = userDefaults.bool(forKey: "wasIntroWatched")
        
        guard !wasIntroWatched else { return }
        
        if let pageViewConroller = storyboard?.instantiateViewController(withIdentifier: "pageViewController") as? PageViewController {
            present(pageViewConroller, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case self.tableView:
            return self.gitHub.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)!
        cell.textLabel?.text = self.gitHub[indexPath.row]
        cell.textLabel?.font = UIFont.appFont(.sfProTextBold(size: 17))
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func showVc(from indexPath: IndexPath) {
        switch indexPath {
        case [0, 0]:
            performSegue(withIdentifier: "searchUserSegue", sender: nil)
        case [0, 1]:
            performSegue(withIdentifier: "searchRepositoriesSegue", sender: nil)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showVc(from: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}






