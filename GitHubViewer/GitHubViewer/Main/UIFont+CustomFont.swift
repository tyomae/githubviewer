//
//  UIFont+CustomFont.swift
//  GitHubViewer
//
//  Created by Артем  Емельянов  on 23/05/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import UIKit

extension UIFont {
    enum FontType {
        case sfProDisplayBold(size: CGFloat)
        case sfProTextBold(size: CGFloat)
        case sfProTextRegular(size: CGFloat)
    }
    
    static func appFont(_ fontType: FontType) -> UIFont {
        switch fontType {
        case let .sfProDisplayBold(size):
            return UIFont(name: "SFProDisplay-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
        case let .sfProTextBold(size):
            return UIFont(name: "SFProText-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
        case let .sfProTextRegular(size):
            return UIFont(name: "SFProText-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
}

