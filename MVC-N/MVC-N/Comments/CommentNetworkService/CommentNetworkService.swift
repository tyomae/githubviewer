//
//  CommentNetworkService.swift
//  MVC-N
//
//  Created by Артем  Емельянов  on 07/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation


struct CommentNetworkService {
    
        static func getComments(competion: @escaping(GetCommentResponse) -> ()) {
            guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts/1/comments") else { return }
            
            NetworkService.shared.getData(url: url) { (json) in
                do {
                    let response = try GetCommentResponse(json: json)
                    competion(response)
                } catch {
                    print(error)
                }
            }
        }
}
