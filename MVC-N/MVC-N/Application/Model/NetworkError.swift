//
//  NetworkError.swift
//  MVC-N
//
//  Created by Артем  Емельянов  on 07/04/2019.
//  Copyright © 2019 Artem Emelianov. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case failInternetError
    case noInternetConnection
}
